
Clover EFI installer log - Wed Feb 19 18:16:02 PST 2020
Installer version: v2.5k r5104 EFI bootloader
======================================================
/dev/disk0 (internal, physical):
   #:                       TYPE NAME                    SIZE       IDENTIFIER
   0:      GUID_partition_scheme                        *512.1 GB   disk0
   1:           Windows Recovery                         523.2 MB   disk0s1
   2:                        EFI NO NAME                 104.9 MB   disk0s2
   3:         Microsoft Reserved                         16.8 MB    disk0s3
   4:       Microsoft Basic Data                         510.9 GB   disk0s4
   5:           Windows Recovery                         604.0 MB   disk0s5

/dev/disk1 (internal, physical):
   #:                       TYPE NAME                    SIZE       IDENTIFIER
   0:      GUID_partition_scheme                        *4.0 TB     disk1
   1:         Microsoft Reserved                         16.8 MB    disk1s1
   2:       Microsoft Basic Data 4TB                     4.0 TB     disk1s2

/dev/disk2 (external, physical):
   #:                       TYPE NAME                    SIZE       IDENTIFIER
   0:      GUID_partition_scheme                        *240.1 GB   disk2
   1:                        EFI EFI                     209.7 MB   disk2s1
   2:                  Apple_HFS Mojave                  139.8 GB   disk2s2
   3:                 Apple_Boot Recovery HD             650.0 MB   disk2s3
   4:                 Apple_APFS Container disk3         99.2 GB    disk2s4

/dev/disk3 (synthesized):
   #:                       TYPE NAME                    SIZE       IDENTIFIER
   0:      APFS Container Scheme -                      +99.2 GB    disk3
                                 Physical Store disk2s4
   1:                APFS Volume cat - 資料              6.4 GB     disk3s1
   2:                APFS Volume Preboot                 82.9 MB    disk3s2
   3:                APFS Volume Recovery                526.6 MB   disk3s3
   4:                APFS Volume VM                      18.3 GB    disk3s4
   5:                APFS Volume Catalina10153           11.2 GB    disk3s5

======================================================
Backing up EFI files

Backing up stage2 file /Volumes/Mojave/EFIROOTDIR/boot  to /Volumes/Mojave/EFI-Backups/r5093/2020-02-19-18h16/boot
Backing up /Volumes/Mojave/EFIROOTDIR/EFI folder to /Volumes/Mojave/EFI-Backups/r5093/2020-02-19-18h16/EFI
======================================================
Installing BootSectors/BootLoader

Stage 0 - Don't write any of boot0af, boot0md, boot0ss to /Volumes/Mojave
Stage 1 - Don't write any of boot1h2, boot1f32alt, boot1xalt to /Volumes/Mojave

======================================================
=========== Clover EFI Installation Finish ===========
======================================================
